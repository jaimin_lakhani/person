/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package person;

/**
 *
 * @author jaiminlakhani
 */
public class TestPerson {
    public static void main(String[] args) {
        Person person1 = new Person("Jaimin", 82, 172);
        System.out.println("Name: "+person1.getName());
        System.out.println("Height: "+person1.getHeight());
        System.out.println("Weight: "+person1.getWeight());
        
        Person person2 = new Person("John", 152, 5.10);
        System.out.println("Name: "+person2.getName());
        System.out.println("Height: "+person2.getHeight());
        System.out.println("Weight: "+person2.getWeight());
    }
}
